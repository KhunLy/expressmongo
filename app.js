const express = require('express');

// permet d'utiliser await/async sans avoir à ajouter un try catch
require('express-async-errors');

//création du serveur express
const app = express();

// pour le gérer le parse en json du body dans les requetes post put et patch
app.use(express.json());

// définition d'une route
// app.get('/', (req, res) => {
//     res.json({ id:42, nom: 'Ly', prenom:'Khun' });
// });

const routes = require('./routes')
app.use(routes);

// démarrer le seveur express
app.listen(3000, () => {
    console.log('Le seveur a démarré sur le port 3000');
});