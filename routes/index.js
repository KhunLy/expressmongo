const router = require('express').Router();

const AuthorController = require('../controllers/author.controller');

router.route('/author')
    .get(AuthorController.getAll)
    .post(AuthorController.add);

module.exports = router;